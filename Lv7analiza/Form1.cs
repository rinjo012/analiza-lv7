﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lv7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Igrac.prvi = textBox1.Text.ToString();
            Igrac.drugi = textBox2.Text.ToString();
            this.Hide();
            Form2 x = new Form2();
            x.ShowDialog();
            this.Close();    
        }
    }

    public static class Igrac
    {
        public static string prvi="Igrac1";
        public static string drugi = "Igrac2";
        public static int count = 1;
        public static string[] x = new string [9];
        public static int pobjeda = 0;
        public static string pobjednik = "";
    }

}